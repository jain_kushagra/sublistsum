package com.pegasusone.sublistsum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
	
	@RequestMapping(value = "/")
	public String displayLandingPage(Map<String, Object> model) {
		SubList subList = new SubList();
		model.put("subListForm", subList);
		return "index";
	}
	
	@RequestMapping(value = "evaluateSubList", method = RequestMethod.POST)
	public String evaluateSubList(@ModelAttribute("subListForm") SubList subList,
            Map<String, Object> model) {
		
		System.out.println("Number List: " + subList.getNumberList());
		System.out.println("Sum: " + subList.getSum());
		
		Collections.sort(subList.getNumberList());
		List<Integer> outputSubList = new ArrayList<Integer>();
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < subList.getNumberList().size(); i++) {
            int psum = subList.getNumberList().get(i);
            list.add(psum);
            for (int j = i + 1; j < subList.getNumberList().size(); j++) {
                if (psum + subList.getNumberList().get(j) == subList.getSum()) {
                	list.add(subList.getNumberList().get(j));
                	outputSubList.addAll(list);
                	System.out.println(list);
                } else if (psum + subList.getNumberList().get(j) < subList.getSum()) {
                	list.add(subList.getNumberList().get(j));
                	psum = psum + subList.getNumberList().get(j);
                } else {
                	list.clear();
                	break;
                }
            }
        }
		model.put("numberList", subList.getNumberList());
		model.put("sum", subList.getSum());
		model.put("subList", outputSubList);
		return "result";
	}
}
